# Visites

Extrait du code permettant à un membre du laboratoire d'organiser une visite, et aux visiteurs de s'inscrire.

 ## Principe

Un membre du laboratoire souhaite accueillir des visiteurs (réunion, séminaire, etc...). Pour des raisons de sécurité, il peut être amené à fournir la liste des personnes, avec différents niveaux de confidentialité. 


## Organisateur

L'accueillant rentre son login, un bref descriptif, et les dates de l'événement dans une URL du type
http://...../chef
Cette adresse n'est à utiliser qu'en interne au laboratoire (par exemple avec une protection des IP au niveau du serveur Apache)

Il reçoit ensuite deux mails : 
- L'un à renvoyer aux invités avec l'URL pour s'inscrire. 
https://..../info.php?code=dsdsqdqdqs
- L'autre lui permettant de vérifier la liste des inscrits, en ayant que le nom et le prénom. 


Cela créé un fichier dsdsqdqdqs.xml décrivant l'évenement

```
<Vevent>
 <uid>dsdsqdqdqs@login</uid>
 <dtstart>2021-12-12</dtstart>
 <dtend>2021-12-13</dtend>
 <summary>Test</summary>
 <Location>LISN</Location>
 <Organizer>Prénom Nom</Organizer>
</Vevent>
```



 ## Visiteurs

Quand un visiteur clique sur l'URL reçue par email, il est invité à remplir des informations dans un formulaire. Seul le nom est obligatoire.
 https://..../info.php?code=dsdsqdqdqs

Les informations rentrées sont envoyées sur un email visite@xxxx . 
Seuls prénom, nom, code de la réunion sont stockées temporairement afin d'afficher la liste des participants. 

`2021-12-08 15:04:26,341z1459,Scoubi,Dou,visite@xxxx`
